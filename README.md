# Index formatter
Formatter for default directory index (listing files and directories) of web-server GNU Apache.<br />
Allow easy access for files (like local area share or file hosting).<br />
Easy control access to files and directories.<br />

![Preview](preview.png?raw=true "Preview")

## Requirements
 - apache2, with fetures:
 	- rewrite
 	- ssl (optional)
 - php5
 - root of domain path (for rewrite)

## Install on Ubuntu/Debian
```
sudo apt-get install -y apache2 php libapache2-mod-php
sudo a2enmod rewrite ssl
sudo sed -i 's/AllowOverride .*/AllowOverride All/' /etc/apache2/apache2.conf
sudo service apache2 restart

sudo chmod -R 777 /var/www/html
git clone git@github.com:ilmenshik/indexier.git /var/www/html

sudo service apache2 restart
firefox http://localhost/
```

## Main features
 - [x] Forbid and hide files which names starts with dot
 - [x] Lock directory
 	- [x] By password, with file .lock (plain password on first line)
 	- [x] Only localhost, with empty file .lock
 	- [x] Lock recursively (all subdirectories and files)
 	- [x] Allow override password in child directories
 - [x] Re-download (for broken connections) (BUG)
 - [x] Hide directory from listing, with file .hide
 - [x] Admin control
 	- [x] Admin by IP or localhost
 	- [x] Admin can remove files and folders
 	- [x] Admin hint password (low-key)

## UI features
 - [x] Interpret php/html files (not plan text)
 - [x] Show index.php/index.html if exist in dir
 - [x] Preview some formats (like .txt, .html, etc.) instead of download
 - [x] File extension icons (GNU Linux Mint)
 - [x] Sort on fly (without reload page)
 - [x] Three color themes (red, blue and green by default) 
 - [x] Full path address with tree of links (quick jumps)
 - [x] Force download/plain buttons
 - [x] Syntax hightlight for special files (like .ini, .conf, .sh, etc.)


## ToDo
 - [ ] White/Black lists of ip addresses or netmask
 - [ ] Fix sort
 	- [ ] Folder before files
 	- [ ] Default sort
 	- [ ] Caсhe sort (cookies)
 - [ ] Load file on scroll (big files) in parser
 - [ ] Lock file by password (like .key)
 - [ ] Hide files in path (list files in .hide)
 - [ ] Allow create/upload files and folders for admin or specified IP addresses
 - [ ] \(Optional) Allow simple authentification for upload/delete
 - [ ] UI:
 	- [ ] Icons and backgrounds for path which are locked, hidden, or encrypted by password, or any of combination.

## Asknoledge bugs
 - [ ] Troubles with transfer big files (more then 200 Mb)
 - [ ] Something strange with cookies
