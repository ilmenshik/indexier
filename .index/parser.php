<?php

function download_file($filename) {
  define('DOWNLOAD', $filename);
  require_once('download.php');
}

function mime_by_extn($filename) {

    $mime_types = array(

        // text
        'txt' => 'text/plain',
        'htm' => 'text/html',
        'html' => 'text/html',
        'php' => 'text/html',
        'css' => 'text/css',
        'js' => 'application/javascript',
        'json' => 'application/json',
        'go' => 'text/plain',
        'py' => 'text/plain',
        'xml' => 'application/xml',
        'swf' => 'application/x-shockwave-flash',
        'flv' => 'video/x-flv',
        'diff' => 'text/plain',

        // images
        'png' => 'image/png',
        'jpe' => 'image/jpeg',
        'jpeg' => 'image/jpeg',
        'jpg' => 'image/jpeg',
        'gif' => 'image/gif',
        'bmp' => 'image/bmp',
        'ico' => 'image/vnd.microsoft.icon',
        'tiff' => 'image/tiff',
        'tif' => 'image/tiff',
        'svg' => 'image/svg+xml',
        'svgz' => 'image/svg+xml',

        // archives
        'zip' => 'application/zip',
        'rar' => 'application/x-rar-compressed',
        'exe' => 'application/x-msdownload',
        'msi' => 'application/x-msdownload',
        'cab' => 'application/vnd.ms-cab-compressed',

        // audio/video
        'mp3' => 'audio/mpeg',
        'qt' => 'video/quicktime',
        'mov' => 'video/quicktime',

        // adobe
        'pdf' => 'application/pdf',
        'psd' => 'image/vnd.adobe.photoshop',
        'ai' => 'application/postscript',
        'eps' => 'application/postscript',
        'ps' => 'application/postscript',

        // ms office
        'doc' => 'application/msword',
        'rtf' => 'application/rtf',
        'xls' => 'application/vnd.ms-excel',
        'ppt' => 'application/vnd.ms-powerpoint',

        // open office
        'odt' => 'application/vnd.oasis.opendocument.text',
        'ods' => 'application/vnd.oasis.opendocument.spreadsheet',

        // fonts
        'svg' => "image/svg+xml",
        'ttf' => "application/x-font-ttf",
        'ttf' => "application/x-font-truetype",
        'otf' => "application/x-font-opentype",
        'woff' => "application/font-woff",
        'woff2' => "application/font-woff2",
        'eot' => "application/vnd.ms-fontobject",
        'sfnt' => "application/font-sfnt",
    );

    $extn=pathinfo($filename, PATHINFO_EXTENSION);

    if (array_key_exists($extn, $mime_types)) {
        return $mime_types[$extn];
    } elseif (function_exists('finfo_open')) {
        $finfo = finfo_open(FILEINFO_MIME);
        $mimetype = finfo_file($finfo, $filename);
        finfo_close($finfo);
        return $mimetype;
    } else {
        return 'application/octet-stream';
    }
}

function file_show_plain($path_to_file){
    $filename=substr(strrchr($path_to_file, "/"), 1);
    $extn=pathinfo($path_to_file, PATHINFO_EXTENSION);
    //$mimeType = 'application/octet-stream';
    //$mimeType = 'text/plain; charset=UTF-8';

    if(!file_exists($path_to_file)){
        header ("HTTP/1.0 404 Not Found");
        return;
    }


    $size=filesize($path_to_file);
    $time=date('r',filemtime($path_to_file));

    $fm=@fopen($path_to_file,'rb');
    if(!$fm) {
        header ("HTTP/1.0 505 Internal server error");
        return;
    }

    $mimeType = mime_by_extn($path_to_file);

    $begin=0;
    $end=$size;

    if(isset($_SERVER['HTTP_RANGE'])) {
        if(preg_match('/bytes=\h*(\d+)-(\d*)[\D.*]?/i', $_SERVER['HTTP_RANGE'], $matches)) {
            $begin=intval($matches[0]);
            if(!empty($matches[1])) $end=intval($matches[1]);
        }
    }


    if($begin>0||$end<$size)
        header('HTTP/1.0 206 Partial Content');
    else
        header('HTTP/1.0 200 OK');


        header("Content-Type: $mimeType");
        header('Cache-Control: public, must-revalidate, max-age=0');
        header('Pragma: no-cache');
        header('Accept-Ranges: bytes');
        header('Content-Length:'.($end-$begin));
        header("Content-Range: bytes $begin-$end/$size");
        header("Content-Disposition: inline; filename='$filename'");
        header("Content-Transfer-Encoding: binary\n");
        header("Last-Modified: $time");
        header('Connection: close');

        $cur=$begin;
        fseek($fm,$begin,0);

        while(!feof($fm)&&$cur<$end&&(connection_status()==0)) {
            print fread($fm,min(1024*16,$end-$cur));
            $cur+=1024*16;
        }
}

function file_force_download($path_to_file) {
  if (file_exists($path_to_file)) {
    #download_file($path_to_file);
    #exit;
    if (ob_get_level()) {
      ob_end_clean();
    }



    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: inline;filename="' . basename($path_to_file).'"');
    //header('Content-Disposition: attachment; filename=' . basename($path_to_file));
    header('Content-Transfer-Encoding: chunked');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($path_to_file));

    //ob_end_clean();
    //flush();
    readfile($path_to_file);
    exit;
  }
}

function file_show_format($path_to_file) {
    // if no formatter
        //file_show_plain($path_to_file);
    //else
        //use_formatter($file,format)


        head('','body_file_formatter');
        #$path_to_file
        echo '<pre class="file_formatter">';

        $extn=strtolower(pathinfo($path_to_file, PATHINFO_EXTENSION));
        $mimeType = mime_content_type($path_to_file);
        $codestyle='log';

        /*
        # https://prismjs.com/download.html#themes=prism-coy&languages=markup+css+clike+javascript+apacheconf+arduino+bash+batch+c+cpp+csp+css-extras+diff+docker+gcode+git+go+groovy+http+hpkp+hsts+ini+java+json+json5+kotlin+lua+makefile+markdown+markup-templating+nginx+nsis+php+plsql+powershell+properties+puppet+python+jsx+tsx+ruby+sql+typescript&plugins=line-numbers+autolinker+toolbar+copy-to-clipboard+match-braces+diff-highlight
        */

        if ($extn == 'xml') $codestyle='xml';
        if (preg_match('/(sh|bash|shell|bashrc)/', $extn)) $codestyle='bash';
        if (preg_match('/(ps1|psm1)/', $extn)) $codestyle='powershell';
        if (preg_match('/(bat|cmd|run)/', $extn)) $codestyle='batch';
        if (preg_match('/(json)/', $extn)) $codestyle='json';
        if (preg_match('/(py)/', $extn)) $codestyle='python';
        if (preg_match('/(h)/', $extn)) $codestyle='c';
        if (preg_match('/(ino)/', $extn)) $codestyle='arduino';
        if (preg_match('/(rb)/', $extn)) $codestyle='ruby';
        if (preg_match('/(java|class)/', $extn)) $codestyle='java';
        if (preg_match('/(conf)/', $extn)) $codestyle='nginx';
        if (preg_match('/(sql)/', $extn)) $codestyle='sql';
        if (preg_match('/(groovy)/', $extn)) $codestyle='groovy';
        if (preg_match('/(go)/', $extn)) $codestyle='go';
        if (preg_match('/(md)/', $extn)) $codestyle='markdown';
        if (preg_match('/(yaml|yml)/', $extn)) $codestyle='yaml';
        if (preg_match('/(diff|dif)/', $extn)) $codestyle='diff';
        if (preg_match('/(css)/', $extn)) $codestyle='css';
        if (preg_match('/(lua)/', $extn)) $codestyle='lua';
        if (preg_match('/(properties)/', $extn)) $codestyle='properties';
        if (preg_match('/(ini)/', $extn)) $codestyle='ini';
        if (preg_match('/(ncc)/', $extn)) $codestyle='gcode';
        if (preg_match('/(nsi)/', $extn)) $codestyle='nsis';
        if (preg_match('/(txt|todo)/', $extn)) $codestyle='markdown';

        if (basename($path_to_file) == 'Dockerfile') $codestyle='docker';
        if (basename($path_to_file) == 'Makefile') $codestyle='makefile';
        if (basename($path_to_file) == 'bashrc') $codestyle='bash';
        if (basename($path_to_file) == 'htaccess') $codestyle='htaccess';
        //if (preg_match('/(todo)/', $extn)) $codestyle='properties';

        $line = fgets(fopen($path_to_file, 'r'));

        $cont = file_get_contents($path_to_file);
        $cont = preg_replace("/<(\/)?([^\/>]*)>/",'&lt;$1$2&gt;',$cont);
        echo '<code class="language-'.$codestyle.' line-numbers">'.$cont.'</code>';
        echo '</pre>';

        echo '<div class="file_formatter_panel">';
            echo '<a href="?download" title="Download file"><div class="file_formatter_button file_formatter_download"><span class="fa fa-arrow-down"></span></div></a>';
            echo '<a href="?plain" title="Show plain text"><div class="file_formatter_button file_formatter_plain"><span class="fa fa-file"></span></div></a>';
            echo '<div class="file_formatter_button file_formatter_zoom" title="Fullscreen mode"><span class="fa fa-arrows-alt"></span></div>';
        echo '<div class="c"></div>';
        echo '</div>';

        echo '<div class="preloader">Loading...</div>';

        foot();
}


function file_show_smart($path_to_file, $force_download=false, $force_plain=false, $filename='') {
        $extn=strtolower(pathinfo($path_to_file, PATHINFO_EXTENSION));
        $mimeType = mime_content_type($path_to_file);

        if ($extn == 'php') {
            require_once($path_to_file);
            exit;
        }

        if ($force_download) file_force_download($path_to_file);


        $canFormatByMime = in_array($mimeType,array(
            'text/plain',
        ));

        $canFormatByName = in_array(basename($path_to_file),array(
            'Dockerfile',
            'Makefile',
            'bashrc',
            'htaccess',
        ));

        $canFormatByExtn = in_array($extn,array(
            'txt',
            'md',
            'xml',
            'log',
            'sh',
            'bash',
            'bat',
            'cmd',
            'groovy',
            'go',
            'py',
            'diff',
            'ncc',
            'java',
            'conf',
            'sql',
            'properties',
            'h',
        ));

        $canShowPlainByExtn = in_array($mimeType,array(
            'mp3',
            'css',
            'js',
            'jpg',
            'png',
            'gif',
            'jpeg',
            'ttf',
            'woff',
            'woff2',
            'eot',
            'svg',
            'php',
            'html',
            'groovy',
        ));

        $canShowPlainByMime = in_array($extn,array(
            'mp3',
            'css',
            'js',
            'jpg',
            'png',
            'gif',
            'jpeg',
            'ttf',
            'woff',
            'woff2',
            'eot',
            'svg',
            'php',
            'html',
            'groovy',
        ));

        if (!$force_plain) {
            $force_plain = (in_array($extn, array('js','css',)) || preg_match('/^image\//', $mimeType));
        }

        $canFormatByMime = in_array($mimeType,array(
            'text/plain',
        ));

        if ($canFormatByMime || $canFormatByExtn || $canFormatByName)
            if ($force_plain)
                file_show_plain($path_to_file);
            else
                file_show_format($path_to_file);
        elseif ($canShowPlainByMime || $canShowPlainByExtn)
            file_show_plain($path_to_file);
        else
            file_force_download($path_to_file);

        exit;
}

?>
