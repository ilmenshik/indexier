<?php
	function human_readable_filesize($bytes, $decimals = 0) {
		$sz = 'BKMGTP';
		$factor = floor((strlen($bytes) - 1) / 3);
		return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . ' ' . @$sz[$factor] . (@$sz[$factor] != 'B' ? 'b' : 't');
	}


	function pretty_filesize($file) {
		$size=filesize($file);
		//return human_readable_filesize($size);

		if($size<1024){$size=$size." Bytes";}
		elseif(($size<1048576)&&($size>1023)){$size=round($size/1024, 1)." Kb";}
		elseif(($size<1073741824)&&($size>1048575)){$size=round($size/1048576, 1)." Mb";}
		else{$size=round($size/1073741824, 1)." Gb";}
		return $size;
	}


	function getUserIP() {
		$client  = @$_SERVER['HTTP_CLIENT_IP'];
		$forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
		$remote  = $_SERVER['REMOTE_ADDR'];

		if(filter_var($client, FILTER_VALIDATE_IP)) {
			$ip = $client;
		} elseif(filter_var($forward, FILTER_VALIDATE_IP)) {
			$ip = $forward;
		} else {
			$ip = $remote;
		}

		return $ip;
	}
	function redirect($location, $script = false) {
		if (!$script) {
			header("Location: ".str_replace("&amp;", "&", $location));
			exit;
		} else {
			echo "<script type='text/javascript'>document.location.href='".str_replace("&amp;", "&", $location)."'</script>\n";
			exit;
		}
	}

	function rmdir_recursive($dir) {
		foreach(scandir($dir) as $file) {
			if ('.' === $file || '..' === $file) continue;
			if (is_dir("$dir/$file")) rmdir_recursive("$dir/$file");
			else @unlink("$dir/$file");
		}
		@rmdir($dir);
	}

	function find_system_file($file_name, $search_path='', $max_depth=20) {
		if ($search_path == '') $search_path = PAGE;
		$file=$search_path . '/'. $file_name;
		$i=0;

		if (file_exists($file)) return $file;

		while (true) {
			$search_path = preg_replace('/\/+$/', '', $search_path);
			$file = ROOT . $search_path . '/'.$file_name;
			//echo 'DEBUG: '.$file .' ('.(file_exists($file) ? 'Exist' : 'Not found').')<br />';
			if (file_exists($file)) break;

			$search_path = dirname($search_path);
			$file = $search_path.'/'.$file_name;


			$i++;
			if ($i>$max_depth || $search_path == '/') {
				return;
				break;
			}
		}
		return $file;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// THEME


	function head($info='',$class='') {
		$pref = PAGE;
		echo '<!doctype html>';
		echo '<html>';
		echo '<head>';
		echo '<meta charset="UTF-8">';
		echo '<title>Share &ndash; /root'.$pref.'</title>';
		echo '<link rel="shortcut icon" href="/.index/img/favicon.ico">';

		echo '<link rel="stylesheet" href="/.index/css/style.css">';
		if (THEME != 'default' && THEME != '') {
			$theme = '/.index/css/style_'.THEME.'.css';
			if (file_exists(ROOT . $theme)) echo '<link rel="stylesheet" href="'.$theme.'" />';
			$exten = '/.index/css/exten_'.THEME.'.css';
			if (file_exists(ROOT . $exten)) echo '<link rel="stylesheet" href="'.$exten.'" />';

		}
		echo '<link rel="stylesheet" href="/.index/css/font-awesome.css" />';
		echo '<link rel="stylesheet" href="/.index/css/jquery-ui.css">';
		echo '</head>';
		//echo '<div id="snowsnowsnow" style="position:fixed;left:0;top:0;bottom:0;right:0;z-index:999999;pointer-events:none"></div>';
		//echo '<script src="/.index/js/snow.js"></script>';

		echo '<body'.($class != '' ? ' class="'.$class.'"' : '').'>';
		//echo '<div class="container'.(isset($_GET['full']) ? ' file_formatter_max' : '').'">';
		echo '<div class="container">';


		echo '<div class="mainmenu">';
		echo '<a href="/" class="rootlink">ROOT</a>';
		$e = explode('/', $pref);
		$pref='/';
		foreach ($e as $spage) {
			if ($spage == '') continue;
			$pref .= $spage.'/';
			echo ' / <a href="'.$pref.'">'.$spage.'</a>';
		}
		if ($info) echo '<div class="info">'.$info.'</div>';
		echo '</div>';
		echo '<hr />';
	}

	function foot() {
		echo '</div>';
		echo '<div class="footer">Copyright &copy; '.date('Y').' index formatter <a href="mailto:ilmenshik@gmail.com">ilmenshik</a></div>';
		echo '<div class="footer ipaddress">'.USER_IP.(iADMIN ? ' &ndash; ADMIN' : '').' </div>';

		echo '<script src="/.index/js/sorttable.js"></script>';

		echo '<script src="/.index/js/jquery-1.10.2.min.js"></script>';
		echo '<script src="/.index/js/jquery-ui-1.12.0.min.js"></script>';


		echo '<script src="/.index/js/parser.js"></script>';
		echo '<link rel="stylesheet" href="/.index/css/prism.css">';
		echo '<link rel="stylesheet" href="/.index/css/prism-line-numbers.css">';
		echo '<link rel="stylesheet" href="/.index/css/prism-custom.css">';
		echo '<script src="/.index/js/prism.js"></script>';
		echo '<script src="/.index/js/prism-line-numbers.js"></script>';
		echo '<script src="/.index/js/prism-custom.js"></script>';
		echo '</body>';
		echo '</html>';
	}


	function show_error_page($status=0) {
			if ($status == 0) $status=$_SERVER['REDIRECT_STATUS'];
			$codes=array(
				204 => array('401 Content not allowed', 'Content on this page now allowed for you. Sorry.'),
				400 => array('400 Bad Request', 'The request cannot be fulfilled due to bad syntax.'),
				401 => array('401 Content not allowed for viewer', 'It appears that the password and/or user-name you entered was incorrect. <a href="#" onclick="window.location.reload()">Click here</a> to return to the login page.'),
				403 => array('403 Forbidden', 'The server has refused to fulfill your request.'),
				404 => array('404 Not Found', 'Whoops, sorry, but the document you requested was not found on this server.'),
				405 => array('405 Method Not Allowed', 'The method specified in the Request-Line is not allowed for the specified resource.'),
				408 => array('408 Request Timeout', 'Your browser failed to send a request in the time allowed by the server.'),
				414 => array('414 URL To Long', 'The URL you entered is longer than the maximum length.'),
				500 => array('500 Internal Server Error', 'The request was unsuccessful due to an unexpected condition encountered by the server.'),
				502 => array('502 Bad Gateway', 'The server received an invalid response from the upstream server while trying to fulfill the request.'),
				504 => array('504 Gateway Timeout', 'The upstream server failed to send a request in the time allowed by the server.'),
			);

			$errortitle = @($codes[$status][0]);
			if ($errortitle!='')$errortitle=' - '.$errortitle;
			$message = @($codes[$status][1]);

			echo '<div class="error_page">';
			echo '<h1>Oops, error'.$errortitle.'</h1>';
			$img = '/.index/img/'.$status.'.png';
			if (file_exists(ROOT . $img)) {
				echo '<div class="error_image"><img src="'.$img.'" alt="'.$status.'" /></div>';
			}
			echo '<div class="error_text">'.$message.'</div>';
			echo '</div>';
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Authorization

	function getLock($path='') {
		$locked = false;
		$password = '';
		if (!$path) $path = PATH;
		$passfile = find_system_file('.lock',$path);
		if (file_exists($passfile) && is_file($passfile)) {
			$locked=true;
			$password=preg_replace('/\s|\t/','',file_get_contents($passfile));
		}
		return (object) array('locked' => $locked, 'password' => $password);
	}

	function checkPassword($path='') {
		if (!iADMIN) {
			$lock=getLock();
			$password = $lock->password;
			if ($password) {

				if (isset($_GET['logout'])) {
					setcookie('password','',-1);
					unset($_COOKIE);
				}

				if (!isset($_COOKIE['password']) || md5($password) != $_COOKIE['password']) {
					$exit = true;
					$error='';

					if (isset($_POST['password'])) {
						sleep(2);
						if ($password == $_POST['password']) {
							setcookie('password',md5($_POST['password']),strtotime("+1 day"),urlencode(PAGE));
							$exit = false;
							redirect(PAGE);
						} else {
							$error='Sorry. Wrong password.';
						}
					}

					if ($exit) {
						head();

						echo '<div class="error_page">';
						echo '<h1>Password is required to access</h1>';
						$img = '/.index/img/password.png';
						if (file_exists(ROOT . $img)) {
							echo '<div class="error_image"><img src="'.$img.'" alt="" /></div>';
						}
						echo '<div class="error_text">'.PAGE.'</div>';

						echo '<p style="color:red;">&nbsp;'.$error.'&nbsp;</p>';
						echo '<div class="form">';
							echo '<form action="'.PAGE.'" method="post">';
								echo '<input class="password" type="password" placeholder="Password" name="password" /><br />';
								echo '<input class="submit" type="submit" name="submit" value="OK" /><br />';
							echo '</form>';
						echo '</div>';
						exit;
					}

				}

			} else {

				head();
				show_error_page(403);
				foot();
				exit;

			}

			return $password;
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ПОЛУЧЕНИЕ ПЕРЕМЕННЫХ И КОНСТАНТ

	define('ROOT',$_SERVER['DOCUMENT_ROOT']);
	define('USER_IP', getUserIP());

	$e = explode('?',urldecode($_SERVER['REQUEST_URI']));
	$page = $e[0];

	$PATH = ROOT . $page;
	define('INC', ROOT.'/.index/');
	require_once(INC . 'config.php');
	require_once(INC . 'file_extensions.php');

	$adm = false;
	$localhost=(isset($_SERVER['SERVER_ADDR']) ? $_SERVER['SERVER_ADDR'] : '127.0.0.1');

	if (isset($_SERVER['HTTP_X_REAL_IP'])) {
		if (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $_SERVER['HTTP_X_REAL_IP'] != $_SERVER['HTTP_X_FORWARDED_FOR']) $localhost = $_SERVER['HTTP_X_FORWARDED_FOR'];
		else $localhost = $_SERVER['HTTP_X_REAL_IP'];
	}

	if ($localhost == USER_IP && LOCALHOST_IS_ADMIN) $adm = true;
	if (in_array(USER_IP,ADMIN_ADDRESS)) $adm = true;

	define('iADMIN',$adm);

	$PATH = preg_replace('/\/\/+/', '/', $PATH);
	$PATH = preg_replace('/\/$/', '', $PATH);
	define('PATH', $PATH);
	define('PAGE', $page);

	if (!file_exists($PATH)) {
		// SHOW ERROR
		head();
		show_error_page(404);
		foot();
		exit;
	}

	$dirinfo='';
	$lock = getLock();
	if ($lock->locked) {
		$pass = checkPassword();
		if ($lock->password && SHOW_PASSWORD) $dirinfo=' <div class="encrypted"><span>Encrypted by</span>: '.$lock->password.'</div>';
	}

	// Allow\deny lists
	$allow_css_regex = '/^\/'.preg_replace('/\./','\.',basename(INC)).'\//';
	$deny_css_regex = '/\/'.preg_replace('/\./','\.',basename(INC)).'\/$/';
	$hidefile = find_system_file('.hide');

	$page_class='';
	if ($lock->locked) {
		if ($lock->password) $page_class='body_dir_password';
		else $page_class='body_dir_locked';
	}
	elseif ($hidefile) $page_class='body_dir_hidden';

	if ( (!iADMIN && HIDE_CONTENT && $hidefile) || (preg_match($deny_css_regex, PAGE) && !iADMIN) ) {
		head();
		show_error_page(204);
		foot();
		exit;
	}

	if (count(ALLOW_ADDRESS)) {
		if (!in_array(USER_IP,ALLOW_ADDRESS) && !preg_match($allow_css_regex, PAGE)) {
			$PATH = ROOT;
			head();
			show_error_page(403);
			foot();
			exit;
		}
	}

	if (count(DENY_ADDRESS)) {
		if (in_array(USER_IP,DENY_ADDRESS) && !preg_match($allow_css_regex, PAGE)) {
			$PATH = ROOT;
			head();
			show_error_page(403);
			foot();
			exit;
		}
	}

	if ($_SERVER['REDIRECT_STATUS'] > 400) {
		head();
		show_error_page($_SERVER['REDIRECT_STATUS']);
		foot();
		exit;
	}


	// Отображение одного файла

	if ( is_file(PATH) ) {
		//if (FORBID_CONTENT) checkPassword();
		require_once('.index/parser.php');
		$force_download = (isset($_GET['download']) ? true : false);
		$force_plain = (isset($_GET['plain']) ? true : false);
		file_show_smart(PATH,$force_download,$force_plain);
		exit;
	}

	if (iADMIN && isset($_GET['del']) && $_GET['del'] != '') {
		$del=ROOT.PAGE.$_GET['del'];
		if (is_file($del)) {
			@unlink($del);
		}
		if (is_dir($del)) {
			rmdir_recursive($del);
		}

		if (isset($_GET['api']) && $_GET['api'] == 'true') exit;
	}

?>
