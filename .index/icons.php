<?php
	head('icons');
?>
<style type"text/css">
	.c { clear:both; }
	.fas-block {
		width:250px;
		float:left;
		margin:5px 10px;
		text-align:left
	}
	.fas-list {
		font-size:14px;
		line-height:14px;
	}
	.fas-td {
		width:30px;
		font-size:26px;
		line-height:26px;
	}
</style>

<?php	
	echo '<section id="top" class="two"> <div class="container">';
	$handle = @fopen(INC."fa_list", "r");
	if ($handle) {
		$fas=array();
		$i=0;
		while (($buffer = fgets($handle, 4096)) !== false) {
			$i++;
			$fa = preg_replace('/^\./','',$buffer);
			$fas[$i]=$fa;
		}
		fclose($handle);
		
		$total=count($fas);
		$limit=60;
		echo '<h2>Total fa: '.$total .'</h2>';
		
		echo '<table class="fas-list"><tr>'; $every=3;
		foreach($fas as $i => $fa) {
			echo '<td class="fas-td"><i class="fa '.$fa.'"></i></td><td><span>'.$fa.'</span></td>';
			if ($i % $every == 0) echo '</tr><tr>';
		}
		echo '</tr></table>';
		
	}
	
	// --CMD --CMD --CMD --CMD --CMD --CMD --CMD --CMD --CMD --CMD --CMD --CMD --CMD --CMD --CMD
?>

<br />
<h2>Commands</h2>

<br />

<table class="fas-list">
<tr>
<td class="fas-td"><i class="fa fa-spinner fa-spin"></i></td><td><span>fa-spin</span></td>
<td class="fas-td"><i class="fa fa-hourglass fa-pulse"></i></td><td><span>fa-pulse</span></td>
<td class="fas-td"><i class="fa fa-star fa-lg"></i></td><td><span>fa-lg</span></td>
</tr>

<tr>
<td class="fas-td"><i class="fa fa-male fa-rotate-90"></i></td><td><span>fa-rotate-90</span></td>
<td class="fas-td"><i class="fa fa-male fa-rotate-180"></i></td><td><span>fa-rotate-180</span></td>
<td class="fas-td"><i class="fa fa-male fa-rotate-270"></i></td><td><span>fa-rotate-270</span></td>
</tr>

<tr>
<td class="fas-td"><i class="fa fa-home fa-inverse"></i></td><td><span>fa-inverse</span></td>
<td class="fas-td"><i class="fa fa-youtube fa-flip-horizontal"></i></td><td><span>fa-flip-horizontal</span></td>
<td class="fas-td"><i class="fa fa-smile-o fa-flip-vertical"></i></td><td><span>fa-flip-vertical</span></td>
</tr>

<tr>
<td class="fas-td"><i class="fa fa-music fa-stack"></i></td><td><span>fa-stack</span></td>
<td class="fas-td"><i class="fa fa-music fa-stack-1x"></i></td><td><span>fa-stack-1x</span></td>
<td class="fas-td"><i class="fa fa-music fa-stack-2x"></i></td><td><span>fa-stack-2x</span></td>
</tr>

<tr>
<td class="fas-td"><i class="fa fa-star fa-lg"></i></td><td><span>fa-lg</span></td>
<td class="fas-td"><i class="fa fa-star fa-fw"></i></td><td><span>fa-fw</span></td>
<td class="fas-td"><i class="fa fa-star fa-border"></i></td><td><span>fa-border</span></td>
</tr>

<tr>
<td class="fas-td"><i class="fa fa-cog"></i></td><td><span>fa</span></td>
<td class="fas-td"><i class="fa fa-cog fa-2x"></i></td><td><span>fa-2x</span></td>
<td class="fas-td"><i class="fa fa-cog fa-3x"></i></td><td><span>fa-3x</span></td>
</tr>

<tr>
<td class="fas-td"><i class="fa fa-cog fa-4x"></i></td><td><span>fa-4x</span></td>
<td class="fas-td"><i class="fa fa-cog fa-5x"></i></td><td><span>fa-5x</span></td>
<td class="fas-td"><i class="fa"></i></td><td><span></span></td>
</tr>



<tr>
<td class="fas-td" colspan="2"><i class="fa fa-cog fa-pull-left">inner</i>outer</td><td><span>fa-pull-left</span></td>
<td class="fas-td" colspan="2" rowspan="2"><ul class="fa fa-cog fa-ul">
<li>list1</li>
<li>list2</li>
<li>list3</li>
</ul></td><td rowspan="2"><span>fa-ul</span></td>
</tr>

<tr>
<td class="fas-td" colspan="2"><i class="fa fa-cog fa-pull-right">inner</i>outer</td><td><span>fa-pull-left</span></td>
</tr>

<!--
.fa-li
.fa-pull-left
.fa-pull-right
-->

</table>


</div></section>
	
<?php
	foot();
?>
