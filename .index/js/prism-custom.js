Prism.languages.log={
	keyword:/ (INFO) /i,
	datetime:/([0-9]{2}:[0-9]{2}:[0-9]{2}(,[0-9]+)?|[0-9]{4}(-[0-9]{2}-| [A-Za-zА-Яа-я]{3} ) [0-9]{2}|[0-9]{2}(-[0-9]{2}-| [A-Za-zА-Яа-я]{3} )[0-9]{4})/i,
	char:/(\n|^)[0-9]+ /i,
	warning:/ (WARN) /i,
	function:/[-a-z0-9]+(?=\()/i,
	punctuation:/[(){};]/
};
