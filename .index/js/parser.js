$(function(){
	function gotoFull() {
		$('.container').toggleClass('file_formatter_max');
		if ($('.container').hasClass('file_formatter_max')) {
			window.location.hash='full'
		} else {
			window.location.hash=''
		}
	}
	$('.preloader').hide();
	$('.file_formatter_zoom').click(function(){
		gotoFull()
	});

	if(window.location.hash) {
		if (window.location.hash.match(/^#full/)) {
			gotoFull()
		}
	}

})
