<?php
	function redirect($location, $script = false) {
		if (!$script) {
			header("Location: ".str_replace("&amp;", "&", $location));
			exit;
		} else {
			echo "<script type='text/javascript'>document.location.href='".str_replace("&amp;", "&", $location)."'</script>\n";
			exit;
		}
	}

	function getUserIP() {
		$client  = @$_SERVER['HTTP_CLIENT_IP'];
		$forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
		$remote  = $_SERVER['REMOTE_ADDR'];

		if(filter_var($client, FILTER_VALIDATE_IP))
		{
			$ip = $client;
		}
		elseif(filter_var($forward, FILTER_VALIDATE_IP))
		{
			$ip = $forward;
		}
		else
		{
			$ip = $remote;
		}

		return $ip;
	}


	if ($_SERVER['SERVER_ADDR'] == getUserIP())	define('iADMIN',true);
	elseif (getUserIP() == '192.168.224.81')	define('iADMIN',true);
	else define('iADMIN',false);



	$files = array();
	$dirs = array();
	$i=0; $j=0;

	function hfs($bytes, $decimals = 0) {
		$sz = 'BKMGTP';
		$factor = floor((strlen($bytes) - 1) / 3);
		return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . ' ' . @$sz[$factor] . (@$sz[$factor] != 'B' ? 'b' : 't');
	}

	function rmdir_recursive($dir) {
		foreach(scandir($dir) as $file) {
			if ('.' === $file || '..' === $file) continue;
			if (is_dir("$dir/$file")) rmdir_recursive("$dir/$file");
			else unlink("$dir/$file");
		}
		rmdir($dir);
	}

	$link = preg_replace('/[?#].*/','',"$_SERVER[REQUEST_URI]");
	$pref = preg_replace('/^\/[\/]*/','',$link);
	$pathlink='';
	$e = explode('/',$link);
	$fp='';

	if (iADMIN && isset($_GET['del'])) {
		$d = $_GET['del'];
		if (is_dir($d)) rmdir_recursive($d);
		elseif (is_file($d)) unlink($d);
	}

	foreach ($e as $n => $p) {
		if ($p != '') {
			$fp.=$p . '/';
			if ($fp == $pref) {
				$pathlink.=' / '.$p.'';
			} else {
				$pathlink.=' / <a href="/'.$fp.'">'.$p.'</a>';
			}
		}
	}

	foreach(glob($pref . "*") as $f) {
		if (is_dir($f)) {
			if ($f != 'css' || $pref != '') {
				$j++;
				$dirs[$j]['path']=basename($f);
				$dirs[$j]['name']=basename($f);
			}
		} else {
			$name = basename($f);
			if ($name != 'index.php' && $name != 'index.css') {
				$i++;
				$files[$i]['file']=basename($f);
				$files[$i]['name']=$name;
				$files[$i]['size']=filesize($f);
				$files[$i]['date']=filemtime($f);
			}
		}
	}




	if (isset($_GET['u']) && preg_match('/^(name|size|date)$/',$_GET['u'])) {
		if (!isset($_COOKIE['sort']) || $_COOKIE['sort'] != $_GET['u']) {
			$_COOKIE['sort'] = $_GET['u'];
			$_COOKIE['reverce'] = 1;
		}

		if ($_COOKIE['sort'] == $_GET['u']) {
			if ($_COOKIE['reverce'] == 0) $_COOKIE['reverce'] = 1;
			else $_COOKIE['reverce'] = 0;
		}
		setcookie('sort',$_COOKIE['sort'],strtotime("+15 minute"),'/');
		setcookie('reverce',$_COOKIE['reverce'],strtotime("+15 minute"),'/');
		redirect('/'.$pref);
	}

	if (isset($_COOKIE['sort'])) setcookie('sort',$_COOKIE['sort'],strtotime("+15 minute"),'/');
	if (isset($_COOKIE['reverce'])) setcookie('reverce',$_COOKIE['reverce'],strtotime("+15 minute"),'/');



	class isort {
		private static $field;
		private static $reverce;
		private static function cmp($a, $b) {
			$x = $a[self::$field];
			$y = $b[self::$field];

			if (self::$reverce) {
				return $res = $x > $y ? -1 : ($x < $y ? 1 : 0);;
			} else {
				return $res = $x > $y ? 1 : ($x < $y ? -1 : 0);
			}
		}
		public static function sort(array &$array, $field, $reverce = false) {
			self::$field = $field;
			self::$reverce = $reverce;
			usort($array, array(__CLASS__, 'cmp'));
		}
	}


    // DEFAULT
    if (!isset($_COOKIE['sort'])) {
        $_COOKIE['sort'] = 'date';
        $_COOKIE['reverce'] = 1;
    }

	if (isset($_COOKIE['sort']) && $_COOKIE['reverce'] != -1) {
		isort::sort($files, $_COOKIE['sort'], $_COOKIE['reverce']);
		if ($_COOKIE['sort'] == 'name') {
			isort::sort($dirs, $_COOKIE['sort'], $_COOKIE['reverce']);
		}
	}


function head($path,$pref) {
	echo '<html>';
	echo '<head>';
	echo '<link href="/css/index.css" media="all" rel="stylesheet" type="text/css" />';
	//echo '<link rel="shortcut icon" href="/css/favicon.ico" type="image/x-icon">';
	echo '<link rel="shortcut icon" href="/css/favicon.png" type="image/png">';
	echo '<title>Share &ndash; /root/'.$pref.'</title>';
	echo '</head>';
	echo '<body>';


	echo '<div class="wrapper">';
	echo '<div class="pathclass"><a href="/">root</a>'.$path.'</div>';

}

function foot() {
	echo '</div>'; // wrapper
	echo '<div class="footer">Copyright &copy; '.date('Y').' indexes formatter <a href="mailto:ilmenshik@gmail.com">ilmenshik</a> '.(iADMIN ? ' - iADMIN' : '').' </div>';
	echo '</body>';
	echo '</html>';
}

/////////////////////  PAGE  /////////////////////////////////////////////////////////////////////////


	if ($i>0) {
		$passfile = $pref . '.pass';
		if (file_exists($passfile)) {
			$password=preg_replace('/\s|\t/','',file_get_contents($passfile));
			$suff = (iADMIN ? ' <div class="enc"><span>Encrypted</span>: '.$password.'</div>' : '');

			if (!isset($_COOKIE['password']) || md5($password) != $_COOKIE['password']) {
				if (isset($_POST['password']) && $password == $_POST['password']) {
					setcookie('password',md5($_POST['password']),strtotime("+1 day"));
					sleep(2);
					redirect('/'.$pref);
					exit;
				}
				head($pathlink.$suff,$pref);
				//head($pathlink,$pref);
				//if (iADMIN) echo '<p>.pass = '.$password.'</p>';
				echo '<div class="" style="background:#dddddd; border:2px gray solid; padding:15px; text-align:center;">';
				echo '<p>Для доступа нужен пароль</p>';
				if (isset($_POST['password']) && $_POST['password'] != '') echo '<p style="color:red;">Сорян! Неверный пароль</p>';

				echo '<form action="/'.$pref.'" method="post">';
				echo '<input type="password" placeholder="Password" name="password" />';
				echo '</form>';
				echo '<br />';
				echo '</div>';
				foot();
				exit;
			} else {
				head($pathlink.$suff,$pref);
			}

		} else {
			head($pathlink,$pref);
		}

		echo '<div class="table">';
		echo '<div class="row header blue">';
			//echo '<div class="cell" style="width:20px;">#</div>';
			function is_filtered($n) {
				if (isset($_COOKIE['sort']) && $_COOKIE['sort'] == $n || (!isset($_COOKIE['sort']) && $n == 'name')) {
					if (isset($_COOKIE['reverce']) && $_COOKIE['reverce'] == 1) return '<img src="/css/sort-desc.png" alt=">" class="orderbtn" />';
					else return '<img src="/css/sort-osc.png" alt=">" class="orderbtn" />';
				}
			}
			echo '<div class="cell"><a href="?u=name">Name</a> '.is_filtered('name').'</div>';
			echo '<div class="cell" style="width:50px;"><a href="?u=size">Size</a> '.is_filtered('size').'</div>';
			echo '<div class="cell" style="width:140px;"><a href="?u=date">Date</a> '.is_filtered('date').'</div>';
			if (iADMIN) echo '<div class="cell" style="width:10px;">&nbsp;</div>';
		echo '</div>';

		if ($pref != '') {
			$icon='/css/back.png';
			echo '<div class="row files">';
			//echo '<div class="cell"></div>';
			echo '<div class="cell"><a href="../"><img src="'.$icon.'" alt="" class="icon" /> ..</a></div>';
			echo '<div class="cell"></div>';
			echo '<div class="cell"></div>';
			if (iADMIN) echo '<div class="cell"></div>';
			echo '</div>';
		}

		$icon='/css/dir.png';
			foreach ($dirs as $i => $d) {
			echo '<div class="row files">';
			//echo '<div class="cell"></div>';
			echo '<div class="cell"><a href="'.$d['path'].'/"><img src="'.$icon.'" alt="" class="icon" /> '.$d['name'].'/</a></div>';
			echo '<div class="cell"></div>';
			echo '<div class="cell"></div>';
			if (iADMIN) echo '<div class="cell"><a href="?del='.$d['path'].'" class="dellink">x</a></div>';
			echo '</div>';
		}


		foreach ($files as $i => $f) {
			echo '<div class="row files">';
			//echo '<div class="cell"></div>';
			$icon='/css/no-ext.png';

			if (preg_match('/\.(png|jpg|gif|jpeg)$/',$f['name'])) $icon = $f['file'];


			echo '<div class="cell"><a href="'.$f['file'].'"><img src="'.$icon.'" alt="" class="icon" /> '.$f['name'].'</a></div>';
			echo '<div class="cell">'.hfs($f['size']).'</div>';
			echo '<div class="cell">'.date("Y-M-d H:i", $f['date']).'</div>';
			if (iADMIN) echo '<div class="cell"><a href="?del='.$f['file'].'" class="dellink">x</a></div>';
			echo '</div>';
		}
		echo '</div>'; // table
	} else {
		if (is_dir($pref)) {
			echo '<h2>Empty</h2>';
		} else {
			echo '<h2>Directory not found!</h2>';
		}
			echo '<a href="../">Go level up...</a>';
	}

	foot();
?>
