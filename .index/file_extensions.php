<?php
$_EXTENSION[''] = '&nbsp;';
$_EXTENSION['png'] = 'PNG Image';
$_EXTENSION['jpg'] = 'JPEG Image';
$_EXTENSION['jpeg'] = 'JPEG Image';
$_EXTENSION['svg'] = 'SVG Image';
$_EXTENSION['gif'] = 'GIF Image';
$_EXTENSION['ico'] = 'Windows Icon';

$_EXTENSION['txt'] = 'Text File';
$_EXTENSION['log'] = 'Log File';
$_EXTENSION['htm'] = 'HTML File';
$_EXTENSION['html'] = 'HTML File';
$_EXTENSION['xhtml'] = 'HTML File';
$_EXTENSION['shtml'] = 'HTML File';
$_EXTENSION['php'] = 'PHP Script';
$_EXTENSION['py'] = 'Python Script';
$_EXTENSION['js'] = 'Javascript File';
$_EXTENSION['css'] = 'Stylesheet';

$_EXTENSION['pdf'] = 'PDF Document';
$_EXTENSION['xls'] = 'Spreadsheet';
$_EXTENSION['xlsx'] = 'Spreadsheet';
$_EXTENSION['doc'] = 'Microsoft Word Document';
$_EXTENSION['docx'] = 'Microsoft Word Document';

$_EXTENSION['htaccess'] = 'Apache Config File';
$_EXTENSION['exe'] = 'Windows Executable';

$_EXTENSION['zip'] = 'Archive';
$_EXTENSION['rar'] = 'Archive';
$_EXTENSION['tar'] = 'Archive';
$_EXTENSION['gz'] = 'Archive';
$_EXTENSION['bz2'] = 'Archive';
$_EXTENSION['bz'] = 'Archive';
$_EXTENSION['tgz'] = 'Archive';
$_EXTENSION['7z'] = 'Archive';

$_EXTENSION['deb'] = 'Package';
$_EXTENSION['rpm'] = 'Package';


$_EXTENSION['doc'] = 'Document Word';
$_EXTENSION['docx'] = 'Document Word';
$_EXTENSION['ppt'] = 'Document Presentation';
$_EXTENSION['pptx'] = 'Document Presentation';
$_EXTENSION['xls'] = 'Document Sheet';
$_EXTENSION['xlsx'] = 'Document Sheet';

$_EXTENSION['odt'] = 'Document Word';
$_EXTENSION['odp'] = 'Document Sheet';

$_EXTENSION['avi'] = 'Video AVI';
$_EXTENSION['wmv'] = 'Video WMV';
$_EXTENSION['mp4'] = 'Video MPEG';
$_EXTENSION['mov'] = 'Video MOV';
$_EXTENSION['m4a'] = 'Video M4A';
$_EXTENSION['mpeg'] = 'Video MPEG';
$_EXTENSION['flv'] = 'Video FLV';
$_EXTENSION['mkv'] = 'Video MKV';
$_EXTENSION['swf'] = 'Video Shockwave';

$_EXTENSION['mp3'] = 'Audio MP3';
$_EXTENSION['ogg'] = 'Audio OGG';
$_EXTENSION['aac'] = 'Audio AAC';
$_EXTENSION['wma'] = 'Audio WMA';
$_EXTENSION['wav'] = 'Audio WAV';

$_EXTENSION['html'] = 'Web page';
$_EXTENSION['htm'] = 'Web page';
$_EXTENSION['xml'] = 'Web list';
$_EXTENSION['php'] = 'Web page';
$_EXTENSION['js'] = 'Web script';

$_EXTENSION['txt'] = 'Text';

$_EXTENSION['bin'] = 'Data file';
$_EXTENSION['dat'] = 'Data file';

$_EXTENSION['crt'] = 'Certificate';
$_EXTENSION['pem'] = 'Certificate';
$_EXTENSION['p12'] = 'Certificate';
$_EXTENSION['key'] = 'Certificate';


$_EXTENSION['sh'] = 'System script';
$_EXTENSION['bash'] = 'System script';
$_EXTENSION['cmd'] = 'System script';
$_EXTENSION['bat'] = 'System script';


$_EXTENSION['msi'] = 'Windows';
$_EXTENSION['exe'] = 'Windows';
$_EXTENSION['pdf'] = 'PDF Document';



?>
