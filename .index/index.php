<?php
	require_once('.index/core2.php');

	// ПОЛУЧЕНИЕ СПИСКА ФАЙЛОВ и КАТАЛОГОВ

	if ( !is_dir(PATH) ) {
		head();
		show_error_page(500);
		foot();
		exit;
	}

	$dir=opendir( PATH );
	while($entryName=readdir($dir)) {
		$dirArray[]=preg_replace('/\/\/+/','/',PATH . '/' . $entryName);
	}
	closedir($dir);
	$dirs = array();
	$files = array();

	foreach ($dirArray as $i => $name) {
		if (is_dir($name)) array_push($dirs, $name);
		if (is_file($name)) array_push($files, $name);
	}

	sort($dirs);
	sort($files);
	$DIRECTORY = array_merge($dirs,$files);
	//echo PATH;
	$php = PATH . '/index.php';
	$html = PATH . '/index.html';
	if (is_file($php)) { require_once($php); exit; }
	elseif (is_file($html)) { require_once($html); exit; }

	// СТРАНИЦА ----------------------------------------------------------------------------------------

	head('',$page_class);
	//top_menu($page,$dirinfo);


	echo '<table class="indexier sortable"><thead>';
	echo '<tr>';
		echo '<th>Filename</th>';
		echo '<th class="file_type">Type</th>';
		echo '<th class="file_size">Size</th>';
		echo '<th class="file_date">Date Modified</th>';
		if (iADMIN) echo '<th class="file_delete"></th>';
	echo '</tr>';
	echo '</thead>';
	echo '<tbody>';

	if (PAGE != '/') {
		//echo '<tfoot>';
		echo '<tr class="sorttop goback">';
		echo '<td><a href=".." class="name">.. (Parent Directory)</a></td>';
		echo '<td><a href="..">&lt;Directory&gt;</a></td>';
		echo '<td>&lt;System Dir&gt;</td>';
		echo '<td>&nbsp;</td>';
		if (iADMIN) echo '<td class="cell">&nbsp;</td>';
		echo '</tr>';

		// Line for reorder odd\even colors in table
		echo '<tr class="sorttop sorttop-even"><td></td></tr>';
	}

	$count = 0;
	foreach ($DIRECTORY as $i => $file) {
		$name=substr(strrchr($file, "/"), 1);

		if (preg_match('/^\./',$name)) {
			if (!iADMIN || $name != '.lock') continue;
		}

		//if ((PAGE == '/' && preg_match('/^\./',$name)) || (PAGE !=	'/' && $name != '..')) {
		//	if (preg_match('/^\./',$name) || (PAGE == '/' && $name == '..')) continue;
		//}

		$favicon="";
		$class="file";
		$link=PAGE . $name;

		$modtime=date("d-M-Y (H:i)", filemtime($file));
		$timekey=date("YmdHis", filemtime($file));

		$hidefile = '';
		if ( is_dir($file) ) {

			$extn="&lt;Directory&gt;";
			$size="&lt;Directory&gt;";
			$sizekey="0";
			$class="dir";

			if(file_exists("$link/favicon.ico")) {
				$favicon=" style='background-image:url($link/favicon.ico);'";
				$extn="&lt;Website&gt;";
			}


			if($name==".") {$name=". (Current Directory)"; $extn="&lt;System Dir&gt;"; $favicon=" style='background-image:url($link/.favicon.ico);'"; }
			if($name=="..") {$name=".."; $extn="&lt;System Dir&gt;";}

			$hidefile = find_system_file('.hide',$file);
			if ($hidefile && !iADMIN) continue;



		} else {
			try {
				$mime=@mime_content_type($file);
				$extn=$mime;
			} catch (Exception $e) {
				$mime='';
			}

			if ($mime == 'text/plain') $extn='Text file';
			elseif (preg_match('/^image\//', $mime)) $extn = 'Image';
			else {
				$extn=pathinfo($file, PATHINFO_EXTENSION);
				if (isset($_EXTENSION[$extn])) $extn = $_EXTENSION[$extn];
				else $extn=strtoupper($extn)." File";
			}

			$size=pretty_filesize($file);
			$sizekey=filesize($file);

		}

		if ($name == '..') $class.=' sorttop';

		$lock = getLock($file);
		if ($lock->locked) {
			if ($lock->password) $class.=' folder_encrypt';
			else $class.=' folder_locked';
		}
		elseif ($hidefile) $class.=' folder_hidden';

		echo '<tr class="'.$class.'">';
		$sort = (is_dir($file) ? 'dir' : 'file');
		echo '<td sorttable_customkey="'.($name == '..' ? '' : $sort).'">';
			echo '<a href="'.$link.'"'.$favicon.' class="name'.($name == '.lock' ? ' sysfile' : '').'">'.$name.($name == '..' ? ' (Parent Directory)' : '').'</a>';
			if ( PREVIEW_IMAGE && $extn == 'Image') {
				echo '<div class="preview_image">';
				echo '<img src="'.$link.'" alt="" />';
				echo '</div>';
			}
		echo '</td>';
		echo '<td sorttable_customkey="'.$sort.'"><a href="'.$link.'">'.$extn.'</a></td>';
		echo '<td sorttable_customkey="'.$sort.'-'.$sizekey.'">'.$size.'</td>';
		echo '<td sorttable_customkey="'.$sort.'-'.$timekey.'">'.$modtime.'</td>';
		if (iADMIN) echo '<td class="cell">'.($name ? '<a href="?del='.basename($link).'" class="dellink">x</a>' : '&nbsp;').'</td>';
		echo '</tr>';

		$count++;
	} // for

	echo '</tbody>';

	echo '</table>';

	if (!$count) {
		echo '<div class="emptytable" colspan="0">Directory is empty</div>';
	}

	foot();
?>
