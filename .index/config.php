<?php
	/*************************************************
			THEME SETTINGS
	*************************************************/

	// Main CSS theme
	// Default: default
	define('THEME', 'urban');

	// Show preview of images on hover them
	// Default: true
	define('PREVIEW_IMAGE', true);

	/*************************************************
			GENERAL CONFIG
	*************************************************/

	// Grant admin rights to host on which the server resides.
	// Default: true
	define('LOCALHOST_IS_ADMIN', false);
	
	// Show password on page for admin or authorized users
	// Default: true
	define('SHOW_PASSWORD', true);

	// Hide content of hidden folder.
	// Default: true
	define('HIDE_CONTENT', true);

	// Forbid content of locked or encrypted by password folder.
	// Default: true
	define('FORBID_CONTENT', true);

	// Confirm of delete
	// 0 - No need confirm 
	// 1 - Confirm only directories
	// 2 - Always need for confirmation
	// Default: 1
	define('CONFIRM_DELETE', 0);

	/*************************************************
			ACCESS LISTS
	*************************************************/

	// White list of IP addresses mode.
	// Will lock all other if not empty!
	define('ALLOW_ADDRESS', array(
		// '192.168.111.111',
	));
	
	// Black list
	// Forbid access for this IP adresses
	define('DENY_ADDRESS', array(
		// '192.168.1.100',
	));

	// Address with admin righs:
	// - can access without password
	// - can access to locked directories
	// - allow upload\remove
	// - can see hidden directories and content of them
	define('ADMIN_ADDRESS', array(
		'127.0.0.1',
	));


?>
