#!/bin/bash
## xml
## txt

## pdf
## doc
## docx
## xls
## xlsx

## dat
## bin

## crt
## key
## pem
## p12

## css
## php
## html
## js

## mp3
## wav
## avi
## mp4
## mpeg

## tar
## gz
## bz
## bz2
## tgz

## deb
## rpm

## sh
## bash
## bat
## cmd

## exe
## msi

egrep '^## ' $0 | while read c ext; do
	touch file_$ext.$ext;
done
