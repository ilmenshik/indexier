;${RequireUnit} dbconfig
${RequireUnit} tomcat


;;------------------------------------------------------------------
;; Installer version set
;;------------------------------------------------------------------

Brandingtext "${APPLICATION_DISPLAY_NAME} installer v${INSTALLER_VERSION}"

;;------------------------------------------------------------------
;; Unit constants
;;------------------------------------------------------------------

!define KERNEL_WEBAPPS "..\..\..\webapps"
!define KERNEL_APPLICATION_URL "http://localhost:$tomcat_web_port/${APPLICATION_URL}"
!define KERNEL_LOG_DIR "$INSTDIR\logs"
!define KERNEL_TMP_DIR "$INSTDIR\tmp"

