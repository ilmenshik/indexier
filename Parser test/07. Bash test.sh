#!/bin/bash
tmp="/tmp/test$RANDOM"
rm "$tmp"

cat "random" | grep 'port' | cut -d '"' -f 2 >> "$tmp"
let "j = 0"

for ((i=10000; 1<100000; i++)) do
    grep "$i" "$tmp" > /dev/null
    retval=$?
    if [ $j -lt 4 ] ; then
		if [ $retval -eq 1 ] ; then
	    	let "j = j + 1"
	    	echo $i
		fi
    else
		break
    fi
done
