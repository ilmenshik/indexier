from re import compile

class MyClass:
    attr = None
    def __init__(self, n):
        self.attr = n

    def __str__(self):
        return str(self.attr)

    def iter():
        if not self.attr:
            return 0
        self.attr += 1
        return self.attr


if __name__ == '__main__':
    o = MyClass(0)
    o.iter()
    o.iter()
    print(o)

